class ContactMailer < ApplicationMailer
	# default to: "jakhon8699@gmail.com"
	# default from: 'no-reply@brrcr.com'

	def contact_email(name, email, message)
		@full_name = name
		@email     = email
		@message   = message

		emails = ["jakhon8699@gmail.com", "rgurdov@gmail.com"]
		mail(to: emails, from: email, subject:"Contact Form Message")
	end
end