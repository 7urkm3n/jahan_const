class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception
   	before_action :default_seo_contents

protected
	def default_seo_contents
		@default_meta_title = "BRR: Best Renovation and Remodeling"
		# @default_meta_title = "BRR Cosntruction and Renovation"
		@default_meta_desc  = "BRR Construction - We offer the best renovating services available in surrounding NYC area."
		@default_meta_keyw  = "best contractor in manhattan, brooklyn, queens, bronx, newyork, new jersey, newjersey, best renovation, high end residential contractor NYC, best general contractor NYC, quality contractor nyc, nyc general contractors, nyc contractor reviews, top general contractors in nyc, commercial contractors nyc, nyc commercial general contractors, quality construction brooklyn newyork, quality contracting, quality contracting services, general contractors"

# "Manhattan, Brooklyn, Queens, Bronx, Construction, Drywall, Framing, Fences, Decks, Tile work, Remodeling, Kitchens, Baths, Painting, Roofing, Wood Floors, Laminate Floors, Tile Floors, Countertops, Home Repairs, Additions, Garage Doors, Window Replacement, Siding, Shingling, Roof Leaks, Appliance Installation, Concrete, Patios, Pavers, Brick, Shower, Design, Consulting, Cabinets, Doors, Power Washing, Refinishing, Staining, Restoration, Renovation, Estimates, Retaining Walls, Insulation, Carpentry, Gutters, Water Damage, Downspouts, Drainage, Bids, General Contractor, Tape and Texture, Formica, Free Estimates, Interior, Exterior, Cement, Doors, Foundation, Repairs, Drywall, Handrails, Dry Rot, Porches, Pillars, Stairs, Shower Doors, Builder"
	end
end




