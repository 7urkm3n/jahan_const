$('.b-contact').submit(function() {
		var $form		= $(this);
		var submitData	= $form.serialize();
		var $email		= $form.find('input[name="contact[email]"]');
		var $name		= $form.find('input[name="contact[full_name]"]');
		var $message	= $form.find('textarea[name="contact[message]"]');
		var $submit		= $form.find('input[name="commit"]');
		var $dataStatus	= $form.find('.data-status');
		
		$email.attr('disabled', 'disabled');
		$name.attr('disabled', 'disabled');
		$message.attr('disabled', 'disabled');
		$submit.attr('disabled', 'disabled');
		
		$dataStatus.show().html('<div class="alert alert-info"><strong>Loading...</strong></div>');
		
		$.ajax({ // Send an offer process with AJAX
			type: 'POST',
			url: '/contact_us',
			beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
			data: submitData + '&action=add',
			dataType: 'html',
			success: function(msg){
				// console.log("Yesysyesyesy", msg);
				
				$email.val('').removeAttr('disabled');
				$name.val('').removeAttr('disabled');
				$message.val('').removeAttr('disabled');
				$submit.removeAttr('disabled');

				$dataStatus.html('<div class="alert alert-success"><strong> Message Sent... </strong></div>').fadeIn();

				// if (parseInt(msg, 0) !== 0) {
				// 	var msg_split = msg.split('|');
				// 	if (msg_split[0] === 'success') {
				// 		$email.val('').removeAttr('disabled');
				// 		$name.val('').removeAttr('disabled');
				// 		$message.val('').removeAttr('disabled');
				// 		$submit.removeAttr('disabled');
				// 		$dataStatus.html(msg_split[1]).fadeIn();
				// 	} else {
				// 		$email.removeAttr('disabled');
				// 		$name.removeAttr('disabled');
				// 		$message.removeAttr('disabled');
				// 		$submit.removeAttr('disabled');
				// 		$dataStatus.html(msg_split[1]).fadeIn();
				// 	}
				// }
			}
		});
		
		return false;
	});

