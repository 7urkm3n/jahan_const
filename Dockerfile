# docker run -d --name brrcr_web_1 --env-file=.env.prod -p 3000:3000 7urkm3n/brrcr sh bin/web

FROM ruby:2.4.4-alpine3.7
MAINTAINER Zack Siri <zack@codemy.net>

RUN apk --update add --virtual build-dependencies \
							   build-base \
                               libxml2-dev \
                               libxslt-dev \
                               sqlite-dev \
                               postgresql-dev \
                               nodejs \
                               tzdata \
                               && rm -rf /var/cache/apk/*

# RUN bundle config build.nokogiri --use-system-libraries

# RUN mkdir /usr/src/app
# WORKDIR /usr/src/app

# COPY . /usr/src/app

# COPY Gemfile /usr/src/app/Gemfile
# COPY Gemfile.lock /usr/src/app/Gemfile.lock


# RUN gem install bundler
# RUN bundle install --path vendor/bundle --without development test doc --deployment --jobs=2
# RUN DB_ADAPTER=nulldb bundle exec rake assets:precompile RAILS_ENV=production


ENV RAILS_ROOT /var/www/

# Creates the directory and all the parents (if they don't exist)
RUN mkdir -p $RAILS_ROOT

# This is given by the Ruby Image.
# This will be the de-facto directory that 
# all the contents are going to be stored. 
WORKDIR $RAILS_ROOT

# We are copying the Gemfile first, so we can install 
# all the dependencies without any issues
# Rails will be installed once you load it from the Gemfile
# This will also ensure that gems are cached and onlu updated when 
# they change.
COPY Gemfile ./
COPY Gemfile.lock ./
# Installs the Gem File.
RUN bundle install

# We copy all the files from the current directory to our
# /app directory
# Pay close attention to the dot (.)
# The first one will select ALL The files of the current directory, 
# The second dot will copy it to the WORKDIR!
COPY . .

